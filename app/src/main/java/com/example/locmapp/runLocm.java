package com.example.locmapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.Vector;

/**
 * This class runs LOCM by creating ssh connection to a computer where the LOCM will be executed.
 * Passes the outputs of LOCM to the ResultDisplay activity.
 *
 * @author  Serawork Wallelign
 * @version 1.0
 * @since   2022-02-01
 */

public class runLocm {
    String username = "username";
    String password = "password";
    String hostname = "192.168.1.32";
    int port = 22;
    Domaindata data;
    List<String> sequence;
    Context context;

    /**
     * Constructor specifying parameters:
     * @param sequence the current action sequence
     * @param data all the domain data
     * @param context the activity that calls the method
     */
    public runLocm(List<String> sequence, Domaindata data, Context context) {
        this.data = data;
        this.sequence = sequence;
        this.context = context;
       }

    /**
     * Runs ssh to connect to the computer that runs LOCM. Saves the current action sequence in the destination computer
     * in a text form which will be used as an input by the LOCM algorithm.
     * @return sets output of the Domaindata to the values returned by the LOCM algorithm
     * @throws Exception
     */
    public Domaindata executeRemoteCommand()
            throws Exception {
        String content = "";
        for (int i=0; i<this.sequence.size();i++){
            content+=this.sequence.get(i);
            content+="\n";
        }
        JSch jsch = new JSch();
        Session session = jsch.getSession(username, hostname,port);
        session.setPassword(password);

        // Avoid asking for key confirmation
        Properties prop = new Properties();
        prop.put("StrictHostKeyChecking", "no");
        session.setConfig(prop);
        session.connect();

        if (!session.isConnected())
            throw new RuntimeException("Not connected to an open session.");
        Channel sftp = session.openChannel("sftp");

        sftp.connect();

        ChannelSftp channelSftp = (ChannelSftp) sftp;
        InputStream stream = new ByteArrayInputStream(content.getBytes());
        String dst = "/home/sera/Documents/LOCM/Prob/"+this.sequence.size()+".txt";
        channelSftp.put (stream, dst);
        // SSH Channel
        ChannelExec channelssh = null;
        channelssh = (ChannelExec) session.openChannel("exec");
        // Execute command
        String script = "/home/sera/Documents/LOCM/run_locm.bash 2";
        channelssh.setCommand(script);
        channelssh.setInputStream(null);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        channelssh.setOutputStream(baos);
        InputStream in = channelssh.getInputStream();
        // 5 seconds timeout channel
        channelssh.connect();

        byte[] buffer = new byte[1024];
        StringBuilder strBuilder = new StringBuilder();
        String line = "";
        while (true){
            while (in.available() > 0) {
                int i = in.read(buffer, 0, 1024);
                if (i < 0) {
                    break;
                }
                strBuilder.append(new String(buffer, 0, i));
            }
            if(line.contains("log out")){
                break;
            }
            if (channelssh.isClosed()){
                break;
            }
            try {
                Thread.sleep(10);
            } catch (Exception ee){}
        }
        //System.out.println(strBuilder.toString());
        InputStream inn = channelSftp.get("/home/sera/Documents/LOCM/output/2-puzzle/Blocksworld.pddl");
        Scanner s = new Scanner(inn).useDelimiter("\\A");
        String result = s.hasNext() ? s.next() : "";
        Vector filelist = channelSftp.ls("/home/sera/Documents/LOCM/output/2-puzzle");
        HashMap<String, byte[]> bm = new HashMap<>();
        for(int i=0; i<filelist.size();i++) {
            ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) filelist.get(i);
            String f1 = entry.getFilename();
            if (f1.contains("png")) {
                Channel sftp1 = session.openChannel("sftp");
                sftp1.connect();
                ChannelSftp channelSftp1 = (ChannelSftp) sftp1;
                String fname = "/home/sera/Documents/LOCM/output/2-puzzle/" + f1;
                InputStream fsm = channelSftp1.get(fname);
                BufferedInputStream bufferedInputStream = new BufferedInputStream(fsm);
                Bitmap bmp = BitmapFactory.decodeStream(bufferedInputStream);
                ByteArrayOutputStream bStream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 100, bStream);
                byte[] byteArray = bStream.toByteArray();
                bm.put(f1, byteArray);
                channelSftp1.exit();

            }
        }
        this.data.output.setFiniteState(bm);
        this.data.output.setDomain(result);
        channelSftp.exit();
        channelssh.disconnect();
        session.disconnect();
        return this.data;
    }
}
