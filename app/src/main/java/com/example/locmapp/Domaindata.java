package com.example.locmapp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class holds all the data in the domain and used to pass data from one activity to another.
 *
 * @author  Serawork Wallelign
 * @version 1.0
 * @since   2022-02-01
 */

public class Domaindata implements Serializable {
    //String problem ="";
    String domainName;
    Map <String, String> positions = new HashMap<>();
    Map <String, String> legos;
    List<ActionObject> domain_actions;
    Map<String, ArrayList<Action>> actionSequences= new HashMap<>();
    LocmOutput output = new LocmOutput();

    /**
     * Class constructor
     */
    public Domaindata(){}

    /**
     * Class constructor specifying the parameters:
     * @param domainName name of the domain
     * @param actionSequences the action sequences that are demonstrated so far
     * @param output contains the learned outputs eg. types, predicates, domain in PDDL and finite state machines
     * @param positions used to define the position of objects in the robot's environment
     * @param legos specifies objects in the robot's environment
     */
    public Domaindata(String domainName, Map<String, ArrayList<Action>> actionSequences, LocmOutput output, Map <String, String> positions, Map <String, String> legos){
        this.domainName = domainName;
        this.actionSequences = actionSequences;
        this.output=output;
        this.positions = positions;
        this.legos=legos;
    }

    /**
     * Sets the name of the domain
     * @param name
     */
    public void setDomainName(String name){this.domainName=name;}

    /**
     *
     * @return the name of the domain
     */
    public String getDomainName(){return this.domainName;}

    /**
     * Sets the action sequences
     * @param actionSequences
     */
    public void setActionSequences(Map<String, ArrayList<Action>> actionSequences){
        this.actionSequences=actionSequences;
    }

    /**
     *
     * @return action sequences
     */
    public Map<String, ArrayList<Action>> getActionSequences() {return this.actionSequences;}

    /**
     *
     * @return the learned output
     */
    public LocmOutput getOutput(){return this.output;}

    /**
     * Sets the learned values like types/sorts, predicates, domain
     * @param output
     */
    public void setOutput(LocmOutput output){this.output=output;}

    /**
     *
     * @return all the objects that existed in the domain
     */
    public Set<String> getObjects() {
        Set<String> allObjList = new HashSet<>();
        for (String k : this.actionSequences.keySet()) {
            ArrayList<Action> seq = new ArrayList<>();
            seq = this.actionSequences.get(k);
            for (int i = 0; i < seq.size(); i++) {
                String act = seq.get(i).getActionName();
                ArrayList<String> param = new ArrayList<>();
                param = seq.get(i).getParameters();
                allObjList.addAll(param);
            }
        }
        return allObjList;
    }

    /**
     *
     * @return the name of all the action sequences
     */
    public List<String> getSeqNames(){
        List<String> namesSeq = new ArrayList<>();
        for(String k:this.actionSequences.keySet())
            namesSeq.add(k);
        return namesSeq;
    }

}
