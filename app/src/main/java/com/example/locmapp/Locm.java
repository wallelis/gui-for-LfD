package com.example.locmapp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * Activity to run LOCM.
 *
 * @author  Serawork Wallelign
 * @version 1.0
 * @since   2022-02-01
 */

public class Locm extends AppCompatActivity {
    private ArrayList<Action> current_seq;
    private Domaindata data1;
    private List<String> sequence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locm);

        Bundle b = this.getIntent().getBundleExtra("BUNDLE");
        if (b!=null)
            data1 = (Domaindata) b.getSerializable("data");
            sequence = (List<String>) b.getSerializable("sequence");
        if (data1.actionSequences!=null){
            current_seq = data1.actionSequences.get("temp");
        }
        
        AsyncTask<String, Void, Domaindata> learnDomain = new AsyncTask<String, Void, Domaindata>() {
            Dialog progress;

            @Override
            protected void onPreExecute() {
                progress = ProgressDialog.show(Locm.this,
                        "LOCM is running", "Please wait...");
                super.onPreExecute();
            }

            @Override
            protected Domaindata doInBackground(String... params) {

                runLocm locm = new runLocm(sequence, data1, Locm.this);
                try {
                    data1 = locm.executeRemoteCommand();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return data1;
            }

            @Override
            protected void onPostExecute(Domaindata result) {
                super.onPostExecute(result);
                progress.dismiss();
                Intent action = new Intent(Locm.this, ResultDisplay.class);
                Bundle b = new Bundle();
                b.putSerializable("data", (Serializable) result);
                action.putExtra("BUNDLE", b);
                startActivity(action);

            }
        };

        if (current_seq!=null){
            learnDomain.execute();
        }

        else {
            AlertDialog alertDialog = new AlertDialog.Builder(Locm.this).create();
            alertDialog.setTitle("Error!");
            alertDialog.setMessage("Action Sequence is empty");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }


    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.lc_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.new_model:
                startActivity(new Intent(this, Recording.class));
                return true;
            case R.id.open_model:
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("file/*");
                startActivity(intent);
                return true;
            case R.id.run:
                startActivity(new Intent(this, test.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}