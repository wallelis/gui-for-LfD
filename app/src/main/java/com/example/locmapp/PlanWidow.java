package com.example.locmapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.Serializable;

/**
 * This activity provides a layout to display the plan, if any, found from the test.
 * It also allows the user to choose to retrain by adding new demonstrations or do more tests on the
 * learned domain.
 *
 * @author  Serawork Wallelign
 * @version 1.0
 * @since   2022-02-01
 */

public class PlanWidow extends AppCompatActivity {
    private Domaindata data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_widow);

        ScrollView planView = findViewById(R.id.vv);
        TextView t = findViewById(R.id.plan);

        Bundle b = this.getIntent().getBundleExtra("BUNDLE");
        if (b!=null) {
            String plan = (String) b.getSerializable("plan");
            data = (Domaindata)b.getSerializable("data");
            t.setText(plan);
        }


        Button testButton = (Button) findViewById(R.id.testButton2);
        Button retrainButton = (Button) findViewById(R.id.retrainButton);

        testButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(PlanWidow.this, test.class);
                Bundle b = new Bundle();
                b.putSerializable("data", (Serializable)data);
                intent.putExtra("BUNDLE", b);
                startActivity(intent);;
            }
        });

        retrainButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(PlanWidow.this, Recording.class);
                Bundle b = new Bundle();
                b.putSerializable("data", (Serializable)data);
                intent.putExtra("BUNDLE", b);
                startActivity(intent);
            }
        });
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.lc_menu, menu);
        return true;
    }
    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        menu.findItem(R.id.open_model).setEnabled(false);
        menu.findItem(R.id.new_model).setEnabled(false);
        menu.findItem(R.id.save_model).setEnabled(true);
        menu.findItem(R.id.run).setEnabled(true);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.new_model:
                startActivity(new Intent(this, Recording.class));
                return true;
            case R.id.run:
                startActivity(new Intent(this, test.class));
                return true;
            case R.id.open_model:
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("file/*");
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}