package com.example.locmapp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
/**
 * This activity displays a layout for the user to define initial and goal state using the predicates
 * learned. Also allows the user to choose a planner to use inorder to find a plan for the given problem.
 *
 * @author  Serawork Wallelign
 * @version 1.0
 * @since   2022-02-01
 */

public class test extends AppCompatActivity {
    private Domaindata data;
    private ArrayAdapter<String> predicateAdapter;
    private List<String> objEntered = new ArrayList<>();
    private ArrayList<String> preds;
    List<Predicate> predicates;
    private ArrayList<String> objects;
    List< String > initialFacts=new ArrayList < String >();
    List< String > goalFacts=new ArrayList < String >();
    private ListView listView;
    private ListView listView2;
    private MyAdapter adapter;
    private MyAdapter adapter2;
    private String prob="";
    private String password = "password";
    private String userName="username";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        Bundle b = this.getIntent().getBundleExtra("BUNDLE");
        if (b!=null)
            data = (Domaindata) b.getSerializable("data");
        TextView tv = findViewById(R.id.probView);
        //tv2 = findViewById(R.id.goalSt);
        listView2= findViewById(R.id.listView4);
        listView = findViewById(R.id.listView3);
        adapter = new MyAdapter(test.this, initialFacts);
        adapter2 = new MyAdapter(test.this, goalFacts);
        listView.setAdapter(adapter);
        listView2.setAdapter(adapter2);
        Button Plan = (Button) findViewById(R.id.planButton);
        Button loadProb = (Button) findViewById(R.id.pbutton);
        Button rename = (Button) findViewById(R.id.rename);

        prob+= "(define (problem)\n(:domain Blocksworld)\n";
        prob+="(:objects";

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            Predicate p;
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                PopupMenu popup;
                popup = new PopupMenu(test.this,listView);
                for (String s : data.output.predicates.keySet()) {
                    String s1 = s.toLowerCase();
                    if (initialFacts.get(i).contains(s1)) {
                        String[] newP = data.output.predicates.get(s);
                        p = getPredicate(initialFacts.get(i));
                        for (int j=0; j< newP.length;j++) {
                            popup.getMenu().add(newP[j]);
                        }
                    }
                }
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item1) {
                        String rePred = item1.getTitle().toString();
                        String oldPred = p.getName();
                        p.rename(rePred.split("\\(" )[0]);
                        data.output.setDomain(data.output.domain.replace(oldPred,p.getName()));
                        initialFacts.set(i, p.getName()+p.params.toString());
                        preds.set(i,p.getName());
                        adapter.notifyDataSetChanged();
                        return true;
                    }
                });
                popup.show();

                return false;
            }
        });

        ActivityResultLauncher<Intent> launchSomeActivity1 = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent dataFromRead = result.getData();
                            Uri uri = dataFromRead.getData();
                            try {
                                InputStream in = getContentResolver().openInputStream(uri);

                                BufferedReader r = new BufferedReader(new InputStreamReader(in));
                                StringBuilder total = new StringBuilder();
                                for (String line; (line = r.readLine()) != null; ) {
                                    total.append(line).append('\n');
                                }
                                r.close();
                                prob = total.toString();
                                tv.setText(prob);

                            }catch (Exception e) {
                            }
                        }
                    }
                });
        rename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initialFacts.clear();
                adapter.notifyDataSetChanged();
            }
        });
        loadProb.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent1 = new Intent(Intent.ACTION_GET_CONTENT);
                Uri uri1= Uri.parse("/SDCARD"); // a directory
                intent1.setDataAndType(uri1, "*/*");
                launchSomeActivity1.launch(Intent.createChooser(intent1, "Open folder"));
            }
        });

        Plan.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                prob+="\n)\n(:init\n";
                for (String intfact:initialFacts){
                    prob+="("+intfact+")\n";
                }
                prob+=")\n(:goal\n(and\n";
                for (String gfact:goalFacts){
                    prob+="("+gfact+")\n";
                }
                prob+=")))";
                tv.setText(prob);

                new AsyncTask<Integer, Void, String>() {
                    @Override
                    protected String doInBackground(Integer... params) {
                        String boas="";
                        try {
                           boas = executeRemoteCommand(userName, password, "129.88.64.116", 22);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        int index = boas.indexOf("log out");
                        return boas;
                    }

                    @Override
                    protected void onPostExecute(String result) {
                        super.onPostExecute(result);
                        //progress.dismiss();
                        Intent intent = new Intent(test.this, PlanWidow.class);
                        Bundle b = new Bundle();
                        b.putSerializable("data", (Serializable)data);
                        b.putSerializable("plan", (Serializable) result);
                        intent.putExtra("BUNDLE", b);
                        startActivity(intent);

                    }
                }.execute(1);
            }
        });
        Spinner inSpinner = (Spinner) findViewById(R.id.spinner);
        Spinner goalSpinner = (Spinner) findViewById(R.id.spinner2);
        predicates = data.output.getPredicates();
        preds = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.predicates)));
        for (Predicate P: predicates){
            preds.add(P.getName());
        }
        initialFacts.add("Long Press To Rename Predicates");
        for (Predicate pre:predicates){
            String p = pre.getName();
            p+=pre.params.toString();
            initialFacts.add(p);
        }
        adapter.notifyDataSetChanged();

        objects = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.obj_array)));
        if (data.actionSequences!=null)
            objects.addAll(data.getObjects());
        predicateAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, preds);
        predicateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        inSpinner.setAdapter(predicateAdapter);
        goalSpinner.setAdapter(predicateAdapter);
        inSpinner.setOnItemSelectedListener(new InitialSpinnerClass());
        goalSpinner.setOnItemSelectedListener(new GoalSpinnerClass1());
    }
    public Predicate getPredicate( String name) {
        for (int i = 0; i < predicates.size(); i++) {
            //String s1 = name.toLowerCase();
            if (name.contains(predicates.get(i).name))
                return predicates.get(i);
        }
        return null;
    }

    /**
     * Makes an ssh connection to a computer to execute a planner in ROSPlan to find a solution for a
     * problem defined by the user
     * or extracted from the robot's perception module (TO DO)
     * @param username username of the destination computer
     * @param password password of the destination computer
     * @param hostname ip address
     * @param port port
     * @return a plan if solution found
     * @throws Exception
     */
    public String executeRemoteCommand(String username, String password, String hostname, int port)
            throws Exception {
        JSch jsch = new JSch();
        Session session = jsch.getSession(username, hostname, port);
        session.setPassword(password);

        // Avoid asking for key confirmation
        Properties prop = new Properties();
        prop.put("StrictHostKeyChecking", "no");
        session.setConfig(prop);
        session.connect();

        if (!session.isConnected())
            throw new RuntimeException("Not connected to an open session.");
        Channel sftp = session.openChannel("sftp");

        sftp.connect();

        ChannelSftp channelSftp = (ChannelSftp) sftp;
        String content = data.output.getDomain();
        InputStream stream = new ByteArrayInputStream(content.getBytes());
        channelSftp.put (stream, "/home/sera/ROSPlan/domain.pddl");
        stream = new ByteArrayInputStream(prob.getBytes());
        channelSftp.put(stream, "/home/sera/ROSPlan/problem.pddl");

        // SSH Channel
        ChannelExec channelssh = null;
        channelssh = (ChannelExec) session.openChannel("exec");
        // Execute command
        String script = "/home/sera/ROSPlan/test.bash";
        channelssh.setCommand(script);
        channelssh.setInputStream(null);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        channelssh.setOutputStream(baos);
        InputStream in = channelssh.getInputStream();
        // 5 seconds timeout channel
        channelssh.connect();

        byte[] buffer = new byte[1024];
        StringBuilder strBuilder = new StringBuilder();
        String line = "";
        while (true){
            while (in.available() > 0) {
                int i = in.read(buffer, 0, 1024);
                if (i < 0) {
                    break;
                }
                strBuilder.append(new String(buffer, 0, i));
            }
            if(line.contains("log out")){
                break;
            }
            if (channelssh.isClosed()){
                break;
            }
            try {
                Thread.sleep(10);
            } catch (Exception ee){}
        }

        InputStream inn = channelSftp.get("/home/sera/ROSPlan/plan.txt");
        Scanner s = new Scanner(inn).useDelimiter("\\A");
        String result = s.hasNext() ? s.next() : "";
        channelSftp.exit();
        channelssh.disconnect();
        session.disconnect();
        System.out.println("Session disconnected");
        return result;
        //return strBuilder.toString();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.lc_menu, menu);
        return true;
    }
    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        menu.findItem(R.id.open_model).setEnabled(false);
        menu.findItem(R.id.new_model).setEnabled(false);
        menu.findItem(R.id.save_model).setEnabled(true);
        menu.findItem(R.id.run).setEnabled(false);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.new_model:
                startActivity(new Intent(this, Recording.class));
                return true;
            case R.id.run:
                startActivity(new Intent(this, test.class));
                return true;
            case R.id.open_model:
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("file/*");
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * This is a spinner to define the initial state
     */
    class InitialSpinnerClass implements AdapterView.OnItemSelectedListener {
        int numObj;
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
            if (!parent.getItemAtPosition(pos).toString().equalsIgnoreCase("Enter facts")) {
                String newString = parent.getItemAtPosition(pos).toString();

                for (Predicate st:predicates){
                    if(newString.equals(st.getName()))
                        numObj = st.params.size();
                }
                if (numObj==0)
                    initialFacts.add(newString);
                else
                    initialFacts.add(newString +"(");
                adapter.notifyDataSetChanged();

                for (int i = 0; i < numObj; i++) {
                    PopupMenu popup;
                    popup = new PopupMenu(test.this, listView);
                    for (String s : objects)
                        popup.getMenu().add(s);

                    int finalI = i;

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item1) {
                            if (!item1.getTitle().toString().equalsIgnoreCase("Enter objects")) {
                                String str = initialFacts.get(initialFacts.size()-1);
                                str += item1.getTitle().toString();
                                prob+=" "+item1.getTitle().toString();
                                if (finalI == 0)
                                    str += ")";
                                else
                                    str += ",";
                                initialFacts.set(initialFacts.size()-1, str);
                                adapter.notifyDataSetChanged();

                            }
                            return false;
                        }
                    });
                    popup.show();
                }

            }

        }
        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    /**
     * This is a spinner to define the goal state
     */
    class GoalSpinnerClass1 implements AdapterView.OnItemSelectedListener {
        int numObj;
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
            if (!parent.getItemAtPosition(pos).toString().equalsIgnoreCase("Enter facts")) {
                String newString = parent.getItemAtPosition(pos).toString();

                for (Predicate st:predicates){
                    if(newString.equals(st.getName()))
                        numObj = st.params.size();
                }
                if (numObj==0)
                    goalFacts.add(newString);
                else
                    goalFacts.add(newString +"(");
                adapter2.notifyDataSetChanged();

                for (int i = 0; i < numObj; i++) {
                    PopupMenu popup;
                    popup = new PopupMenu(test.this, listView2);
                    for (String s : objects)
                        popup.getMenu().add(s);

                    int finalI = i;

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item1) {
                            if (!item1.getTitle().toString().equalsIgnoreCase("Enter objects")) {
                                String str = goalFacts.get(goalFacts.size()-1);
                                str += item1.getTitle().toString();
                                if (finalI == 0)
                                    str += ")";
                                else
                                    str += ",";
                                goalFacts.set(goalFacts.size()-1, str);
                                adapter2.notifyDataSetChanged();

                            }
                            return false;
                        }
                    });
                    popup.show();
                }

            }

        }
        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }
}