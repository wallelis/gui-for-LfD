package com.example.locmapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * This is a custom adapter to display enumerated list in ListView.
 *
 * @author  Serawork Wallelign
 * @version 1.0
 * @since   2022-02-01
 */

class MyAdapter extends ArrayAdapter<String>
{
    Context context;
    List <String> title;


    MyAdapter(Context c, List<String> title)
    {

        super(c, R.layout.activity_my_adapter, title);
        this.context = c;
        this.title=title;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater vi = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View row = vi.inflate(R.layout.activity_my_adapter, parent, false);
        TextView titlee = (TextView) row.findViewById(R.id.tx);

        int pos = position+1;
        titlee.setText(+pos + ". " + title.get(position));
        pos++;
        return row;
    }

}