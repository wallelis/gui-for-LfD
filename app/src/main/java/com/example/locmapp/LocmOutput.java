package com.example.locmapp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
/**
 * This class implements LOCM outputs like learned types, domain in PDDL and finite state machines
 * in graph format
 *
 * @author  Serawork Wallelign
 * @version 1.0
 * @since   2022-02-01
 */

public class LocmOutput implements Serializable {
    HashMap<String, Collection<String>> sort = new HashMap<>();
    HashMap<String, String[]> predicates = new HashMap<>();
    String domain = "";
    HashMap<String, byte[]> finiteState;

    /**
     * Constructor
     */
    public LocmOutput() { }

    /**
     * Constructor specifying parameters sort/type, domain and finite state machine
     * @param sort types in the domain
     * @param dom the learned domain in PDDL
     * @param fsm finite state machine for each type
     */
    public LocmOutput(HashMap<String, Collection<String>> sort, String dom, HashMap<String, byte[]> fsm){
        this.sort=sort;
        this.domain=dom;
        this.finiteState=fsm;
    }

    /**
     *
     * @return the learned types in the domain
     */
    public HashMap<String,Collection<String>> getSort(){return this.sort;}

    /**
     *
     * @return returns the learned domain in PDDL
     */
    public String getDomain(){return this.domain;}

    /**
     *
     * @return returns finite state machines in image format
     */
    public HashMap<String, byte[]> getFiniteState(){return this.finiteState;}

    /**
     * Sets the types of the domain
     * @param srt the types/sorts of the domain
     */
    public void setSort(HashMap<String, Collection<String>> srt) {this.sort=srt;}

    /**
     * Sets the PDDL domain
     * @param dom PDDL domain
     */
    public void setDomain(String dom){if (dom!=null){this.domain=dom;}}

    /**
     * Sets the finite state machines of the domain
     * @param fsm byte array containing the finite state machines for each sort/type
     */
    public void setFiniteState(HashMap<String, byte[]> fsm){this.finiteState=fsm;}

    /**
     *
     * @return all the learned predicates in the domain
     */
    public List<Predicate> getPredicates() {
        List<Predicate> predicates = new ArrayList<>();
        String dom=this.getDomain();
        int idx = dom.indexOf(":predicates");
        idx = dom.indexOf("\n",idx)+1;
        int last = dom.indexOf(":action")-7;
        String[] predi=dom.substring(idx, last).replace(" ","").split("\n");
        for(int i=0; i<predi.length;i++) {
            predi[i] = predi[i].substring(1, predi[i].length() - 1);
            String[] predArray = predi[i].split("\\?");
            String[] parms;
            if (predArray.length<2)
                predicates.add(new Predicate(predArray[0]));
            else{
                parms = new String[predArray.length-1];
                System.arraycopy(predArray, 1, parms, 0, predArray.length-1);
                predicates.add(new Predicate(predArray[0], new HashSet<>(Arrays.asList(parms))));
            }
        }
        return predicates;
    }
}
