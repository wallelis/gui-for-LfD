package com.example.locmapp;

import java.io.Serializable;
import java.util.*;
/**
 * This class implements an Action.
 *
 * @author  Serawork Wallelign
 * @version 1.0
 * @since   2022-02-01
 */
public class Action implements Serializable {
    String name;
    ArrayList<String> parameters;

    /**
     * Class constructor specifying
     * @param name the name of the action
     * @param parameters the name of objects affected by the action
     */
    public Action(String name, ArrayList<String> parameters)
    {
        this.parameters=parameters;
        this.name=name;
    }
    @Override
    /**
     * Compares two Actions
     */
    public boolean equals(Object o) {
        boolean retVal = false;
        if (o instanceof Action){
            Action action = (Action) o;
            retVal = action.name.equals(this.name);
        }
        return retVal;
    }

    /**
     * Gets the name of the action
     * @return name of action
     */
    public String getActionName(){return this.name;}

    /**
     * Sets the name of the action
     * @param name
     */
    public void setActionName(String name){this.name=name;}

    /**
     * Gets the number of objects affected by the action
     * @return
     */
    public int getNoOfParameters(){return this.parameters.size();}

    /**
     * Returns the objects affected by the action
     * @return
     */
    public ArrayList<String> getParameters(){return this.parameters;}

}