package com.example.locmapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
/**
 * This activity provides a layout to view demonstrations performed (TO-DO), options to activate/
 * deactivate LEADThrough mode of the robot, extracts the position information and object
 * identification (TO-DO) from the perception module.
 *
 * @author  Serawork Wallelign
 * @version 1.0
 * @since   2022-02-01
 */

public class Recording extends AppCompatActivity {
    boolean pCurrentlyPlaying = false;
    private Domaindata data;
    boolean status;
    String p1;
    String p2;
    private onPostResult hResult = new onPostResult();
    private String password = "password";
    private String userName="user name";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recording);
        Bundle b = this.getIntent().getBundleExtra("BUNDLE");
        if (b!=null){
            data = (Domaindata) b.getSerializable("data");
            //System.out.println(data.actionSequences.keySet());
        }
        try {
            String r = new getStart().execute("3").get();
            if(r.contains("Inactive"))
                status=false;
            else if(r.contains("Active"))
                status = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        Button Leadactivate = (Button)findViewById(R.id.activate);
        if (!status)
            Leadactivate.setText("Activate");
        else
            Leadactivate.setText("Deactivate");

        TextView textView = (TextView) findViewById(R.id.textView1);
        Timer t = new Timer("hello", true);


        ImageButton startButton = (ImageButton) findViewById(R.id.startButton);
        startButton.setBackgroundResource(R.drawable.start);
        Leadactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!status && Leadactivate.getText().toString().equals("Activate")){
                    try {
                        String r = new getStart().execute("2").get();
                        Leadactivate.setText("Deactivate");
                        status = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else if (status && Leadactivate.getText().toString().equals("Deactivate")) {
                    try {
                        String r = new getStart().execute("0").get();
                        Leadactivate.setText("Activate");
                        status = false;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        startButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int image = pCurrentlyPlaying ? R.drawable.start : R.drawable.stop;
                startButton.setImageResource(image);
                pCurrentlyPlaying = !pCurrentlyPlaying;

                TimerTask tt = new TimerTask() {
                        int minute = 0;
                        int seconds = 0;
                        int hour = 0;

                        @Override
                        public void run() {
                            textView.post(new Runnable() {

                                public void run() {
                                    seconds++;
                                    if (seconds == 60) {
                                        seconds = 0;
                                        minute++;
                                    }
                                    if (minute == 60) {
                                        minute = 0;
                                        hour++;
                                    }
                                    textView.setText(""
                                            + (hour > 9 ? hour : ("0" + hour)) + " : "
                                            + (minute > 9 ? minute : ("0" + minute))
                                            + " : "
                                            + (seconds > 9 ? seconds : "0" + seconds));

                                }
                            });

                        }
                    };

                if (pCurrentlyPlaying) {
                    startButton.setBackgroundResource(R.drawable.stop);
                    String r="";
                    try {
                        r = new getStart().execute("1").get();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //String posi = output.substring(output.indexOf("["));
                    if(data.positions==null){
                        data.positions=new HashMap<>();
                        p1 = "P" + 1;
                        data.positions.put(p1, r);
                    }
                    else if(! data.positions.values().contains(r)){
                        int idx = data.positions.size()+1;
                        p1 = "P" + idx;
                        data.positions.put(p1, r);}
                    else {
                        Set<Map.Entry<String, String>> entries = data.positions.entrySet();
                        for(Map.Entry entry : entries) {
                            if(entry.getValue().equals(r)) {
                                p1 = (String) entry.getKey();
                            }
                        }
                    }
                    t.schedule(tt,0,1000);

                }
                else {
                    startButton.setBackgroundResource(R.drawable.start);
                    String r = null;
                    try {
                        r = new getStart().execute("1").get();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(data.positions==null){
                        p2 = "P" + "1";
                        data.positions.put(p2, r);
                    }
                    else if(! data.positions.values().contains(r) ){
                        int idx = data.positions.size() +1;
                        p2 = "P" + idx;
                        data.positions.put(p2, r);}
                    else {
                        Set<Map.Entry<String, String>> entries = data.positions.entrySet();
                        for(Map.Entry entry : entries) {
                            if(entry.getValue().equals(r)) {
                                p2 = (String) entry.getKey();
                            }
                        }
                    }
                    Intent intent = new Intent(Recording.this, SaveActions.class);
                    Bundle b = new Bundle();
                    b.putSerializable("data", (Serializable) data);
                    b.putSerializable("start", (Serializable) p1);
                    b.putSerializable("end", (Serializable) p2);
                    intent.putExtra("BUNDLE", b);
                    startActivity(intent);
                    Recording.this.finish();
                }
                }
        });
    }

    /**
     * This class creates connection with the robot. It has functionalities to activate/deactivate leadthrough
     * mode, get robot's joint target
     */
    class getStart extends AsyncTask<String, Void, String>{
        @Override
        protected String doInBackground(String... params) {
            String boas="";
            try {
                boas = executeRemoteCommand(userName, password, "130.190.27.40", 22,
                        "/home/sera/Downloads/ABB_RWS_REST_API/activate.bash" + " "+ params[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //System.out.println(boas);
            return boas;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            hResult.setResult(result);
            System.out.println(hResult.getResult());
        }
    };

    /*
    public String getJoint(){
        String key="";
        try {
            String boas = executeRemoteCommand(userName, password, "130.190.27.40", 22,
                    "/home/sera/Downloads/ABB_RWS_REST_API/activate.bash 1");
            String posi = boas.substring(boas.indexOf("["));
            if(!data.positions.values().contains(posi) && data.positions!=null){
                key = "P" + data.positions.size()+1;
                data.positions.put(key, posi);}
            else {
                Set<Map.Entry<String, String>> entries = data.positions.entrySet();
                for(Map.Entry entry : entries) {
                    if(entry.getValue().equals(posi)) {
                        key = (String) entry.getKey();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return key;
    }
     */

    /**
     * This class creates a remote connection to a computer to execute REST_API wtitten in python
     */
    public String executeRemoteCommand(String username, String password, String hostname, int port, String script)
            throws Exception {
        JSch jsch = new JSch();
        Session session = jsch.getSession(username, hostname, port);
        session.setPassword(password);

        // Avoid asking for key confirmation
        Properties prop = new Properties();
        prop.put("StrictHostKeyChecking", "no");
        session.setConfig(prop);
        session.connect();

        if (!session.isConnected())
            throw new RuntimeException("Not connected to an open session.");
        // SSH Channel
        ChannelExec channelssh = null;
        channelssh = (ChannelExec) session.openChannel("exec");
        // Execute command
        //String script = "/home/sera/ROSPlan/test.bash";
        channelssh.setCommand(script);
        channelssh.setInputStream(null);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        channelssh.setOutputStream(baos);
        InputStream in = channelssh.getInputStream();
        // 5 seconds timeout channel
        channelssh.connect();

        byte[] buffer = new byte[1024];
        StringBuilder strBuilder = new StringBuilder();
        String line = "";
        while (true){
            while (in.available() > 0) {
                int i = in.read(buffer, 0, 1024);
                if (i < 0) {
                    break;
                }
                strBuilder.append(new String(buffer, 0, i));
            }
            if(line.contains("log out")){
                break;
            }
            if (channelssh.isClosed()){
                break;
            }
            try {
                Thread.sleep(10);
            } catch (Exception ee){}
        }


        channelssh.disconnect();
        session.disconnect();
        return strBuilder.toString();
    }
    public class onPostResult{
        String output;
        public onPostResult(){}
        public onPostResult(String out){
            this.output=out;
        }
        public String getResult(){return this.output;}
        public void setResult(String result) {
            this.output = result;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.lc_menu, menu);
        return true;
    }
    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        menu.findItem(R.id.new_model).setEnabled(false);
        menu.findItem(R.id.save_model).setEnabled(true);
        menu.findItem(R.id.open).setEnabled(false);
        menu.findItem(R.id.run).setEnabled(false);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.open_model:
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("file/*");
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
